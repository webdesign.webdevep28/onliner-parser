<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TelegraphChat extends Model
{
	protected $table = 'telegraph_chats';

	protected $fillable = [
		'chat_id',
		'name',
		'telegraph_bot_id',
	];

	// If chats belong to a bot:
	public function bot()
	{
		return $this->belongsTo(TelegraphBot::class, 'telegraph_bot_id');
	}
}