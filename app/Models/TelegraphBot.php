<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TelegraphBot extends Model
{
	protected $table = 'telegraph_bots';

	protected $fillable = [
		'token',
		'name',
	];

	// Define a relationship if there's any. For example, if each bot can have many chats:
	public function chats()
	{
		return $this->hasMany(TelegraphChat::class, 'bot_id');
	}
}