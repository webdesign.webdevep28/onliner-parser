<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

	protected $table = 'orders';
	protected $fillable = ['taskNo', 'expired', 'location', 'title', 'description', 'responses_count', 'response_status', 'price'];

	public $timestamps = false;

	public static function getOrder($order_id): ?array
	{
		if (!$order_id) return null;

		return self::where('id', $order_id)->first() ?? [];
	}

	public static function getAllOrders(): array
	{
		return self::all()->toArray();
	}
}

