<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class RouteController extends Controller
{
	public static function renderPage($page, $specificData = [])
	{
		$defaultData = array();

		if (auth()->user() != null) {
			$user_id = auth()->user()->id;
			$currentUser = User::where('id', $user_id)->first();

			$defaultData['user'] = $currentUser;
		}

//		switch ($page) {
//			case 'DashboardPage':
//				break;
//		}

		$fullData = null;

		if (!empty($specificData)) {
			$fullData = array_merge($specificData, $defaultData);
		}

		$data = $fullData ? $fullData : $defaultData;

//		LogService::debug(json_encode($data));

		return Inertia::render($page, $data);
	}

	public function getOrders(Request $request)
	{
		$orders_from_site = OrdersController::parseOrders();
		$orders_from_DB = Order::getAllOrders();

		OrdersController::syncOrders($orders_from_site, $orders_from_DB);

		return self::renderPage('GetOrders',
			[
				'orders_from_site' => $orders_from_site,
				'orders_from_DB' => $orders_from_DB,
			]
		);
	}

	public function refreshOrders(Request $request)
	{
		OrdersController::syncOrders(OrdersController::parseOrders(), Order::getAllOrders());
	}
}