<?php

namespace App\Http\Controllers;

use App\Helpers\DomHelper;
use App\Helpers\TelegramHelper;
use App\Models\Order;
use App\Providers\LogService;
use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use Spatie\Browsershot\Browsershot;

class OrdersController extends Controller
{
	protected static string $parsed_url = 'https://s.onliner.by/tasks?status%5B%5D=active&sections%5B%5D=developer';

	public static function parseOrders(): mixed
	{
		$contents = array();
		$dom = new Dom();
		$html = Browsershot::url(self::$parsed_url)
//			->waitUntilNetworkIdle()
			->bodyHtml();

		$dom->loadStr($html);
		$contents = $dom->find('.service-offers__unit');

		$items = array();

		foreach ($contents as $content) {
			$items[] = [
				'title' => DomHelper::findFirst($content, '.service-offers__name'),
				'taskNo' => DomHelper::getFirstTaskNo($content, '.service-offers__link'),
				'description' => DomHelper::findFirst($content, '.service-offers__description'),
				'created' => DomHelper::findFirst($content, '.service-offers__time > span'),
				'expired' => DomHelper::findFirst($content, '.service-offers__details-item_time'),
				'location' => DomHelper::findFirst($content, '.service-offers__details-item_map-marker'),
				'responses_count' => floatval(DomHelper::findFirst($content, '.service-offers__details-item_tag', '0')),
				'price' => floatval(DomHelper::findFirst($content, '.service-offers__price > span')),
			];
		}

//	???	Maybe, Return orders from onliner.by from the last added, in order to using array_reverse function for returned array
		return array_reverse($items);
	}

	public function getOrders(Request $request)
	{
//		$orders_from_site = OrdersController::parseOrders();
		$orders_from_DB = Order::getAllOrders();

		return RouteController::renderPage('GetOrders',
			[
//				'orders_from_site' => $orders_from_site,
				'orders_from_DB' => $orders_from_DB,
			]
		);
	}

	public static function syncOrders($orders_from_site, $orders_from_DB)
	{
//		$orders_from_site = self::parseOrders();
//		$orders_from_DB = Order::getAllOrders();

		foreach ($orders_from_site as $order_site) {
			$found_item = false;

			foreach ($orders_from_DB as $order_DB) {
				if ($order_site['taskNo'] == $order_DB['taskNo']) {
					$found_item = true;

					break;
				}
			}

			if (!$found_item) {
				LogService::info("Adding an order into DB: {$order_site['taskNo']}");
//				LogService::debug($order_site);
				$order = new Order();

				$order->taskNo=$order_site['taskNo'];
				$order->expired=$order_site['expired'];
				$order->location=$order_site['location'];
				$order->title=$order_site['title'];
				$order->description=$order_site['description'];
				$order->responses_count=$order_site['responses_count'];
				$order->price=$order_site['price'];

				$order->save();

				TelegramHelper::sendNewOrderToChat($order_site);
			}
		}
	}
}