<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;

class RefreshOrders extends Command
{
	protected $signature = 'orders:refresh';
	protected $description = 'Sends an HTTP request to refresh orders from onliner.by.';

	public function __construct()
	{
		parent::__construct();
	}

	public function handle()
	{
		$client = new Client();
		$response = $client->request('GET', 'http://localhost:8000/orders/refresh');

		$this->info('Response: ' . $response->getBody());
	}
}
