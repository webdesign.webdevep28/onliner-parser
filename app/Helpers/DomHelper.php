<?php

namespace App\Helpers;

use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Boolean;

class DomHelper
{
	public static function findFirst($dom, $propName, $defaultValue = ''): ?string
	{
		if ($dom != null) {
			$prop = $dom->find($propName, 0);

			if ($prop != null) {
				return $prop->innerHTML;
			}
		}

		return $defaultValue;
	}

	public static function getFirstAttr($dom, $propName, $attrName, $defaultValue = ''): ?string
	{
		if ($dom != null) {
			$prop = $dom->find($propName, 0);

			if ($prop != null) {
				return $prop->{$attrName} ?? $defaultValue;
			}
		}

		return $defaultValue;
	}

	public static function ifExists($dom, $propName): bool
	{
		if ($dom != null) {
			$prop = $dom->find($propName, 0);

			if ($prop != null) {
				return true;
			}
		}

		return false;
	}

	public static function getFirstTaskNo($dom, $propName) : ?string {
		$first = self::getFirstAttr($dom, $propName, 'href');

		if ($first) {
			return Str::substr($first, 7);
		}

		return '';
	}
}