<?php

namespace App\Helpers;

use App\Models\TelegraphBot;
use App\Models\TelegraphChat;
use App\Providers\LogService;
use Illuminate\Support\Facades\Http;

//use DefStudio\Telegraph\Models as TelegraphModel;
//use DefStudio\Telegraph\Handlers\WebhookHandler;

class TelegramHelper
{
	public static function sendNewOrderToChat($order = null)
	{
		$botId = 1; // Example bot ID
		$bot = TelegraphBot::find($botId)->first();
		$chats = TelegraphChat::where('telegraph_bot_id', $botId)->get();

		if (isset($chats) && !empty($chats) && isset($bot) && !empty($bot) && isset($order) && !empty($order)) {
			$botToken = $bot->token;

			foreach ($chats as $chat) {
				$chatId = $chat->chat_id;

				$response = Http::get("https://api.telegram.org/bot$botToken/sendMessage", [
					'chat_id' => $chatId,
					'text' => "\n ------------------------- \n
						{$order['title']}
						{$order['created']}
						Цена: " . ($order['price'] > 0 ? $order['price'] : "не указана") . "
						{$order['expired']}, {$order['location']}
						Отклики: " . ($order['responses_count'] > 0 ? $order['responses_count'] : "нет откликов") . "			
						{$order['description']}
						https://s.onliner.by/tasks/{$order['taskNo']}",
				]);
			}
		}
	}
}